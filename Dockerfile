FROM ubuntu:latest
MAINTAINER Nitin Jain  <nitin.solna@gmail.com>

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y stress

ENTRYPOINT ["/usr/bin/stress", "--verbose"]
CMD []
